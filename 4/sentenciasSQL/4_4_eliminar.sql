Elimina todos los empleados que tengan un salario superior a 20.000
DELETE employees FROM employees INNER JOIN salaries ON 20000 < salaries.salary AND employees.emp_no=salaries.emp_no

Elimina el departamento que tenga más empleados
DELETE FROM departments WHERE dept_no=(SELECT dept_no FROM departments WHERE dept_no=(SELECT dept_emp.dept_no FROM dept_emp INNER JOIN employees WHERE employees.emp_no=dept_emp.emp_no GROUP BY dept_no ORDER BY count(dept_emp.dept_no) DESC LIMIT 1)
)
