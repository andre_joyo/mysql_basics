Cambia el nombre de un empleado.
Consulta que afecte únicamente a un empleado basándote en su nombre, apellido y fecha de nacimiento.
UPDATE employees
SET first_name = "Anna"
WHERE first_name = "Zulema" AND last_name = "Diaz" AND birth_date = "1991-05-22";


Cambia el nombre de todos los departamentos.
UPDATE departments
SET dept_name = "dept_A"
WHERE dept_name = "deptA";

UPDATE departments
SET dept_name = "dept_B"
WHERE dept_name = "deptB";

UPDATE departments
SET dept_name = "dept_C"
WHERE dept_name = "deptC";

UPDATE departments
SET dept_name = "dept_D"
WHERE dept_name = "deptD";

UPDATE departments
SET dept_name = "dept_E"
WHERE dept_name = "deptE";
