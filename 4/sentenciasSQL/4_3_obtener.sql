Selecciona todos los empleados que tengan un salario superior a 20.000
SELECT * FROM salaries WHERE salary>20000

Selecciona todos los empleados que tengan un salario inferior a 10.000
SELECT * FROM salaries WHERE salary<10000

Selecciona todos los empleados que tengan un salario entre 14.000 y 50.000
SELECT * FROM salaries WHERE salary>14000 AND salary<50000

Selecciona el número total de empleados
SELECT count(emp_no) FROM employees

Selecciona el número total de empleados que han trabajado en más de un departamento
SELECT count(emp_no), emp_no FROM dept_emp GROUP BY emp_no;

Selecciona los títulos del año 2019
SELECT title FROM titles WHERE from_date BETWEEN "2019-01-01" AND "2019-12-31" OR to_date BETWEEN "2019-01-01" AND "2019-12-31" GROUP BY title;

Selecciona únicamente el nombre de los empleados en mayúsculas
SELECT UPPER(RTRIM(first_name)) FROM employees;

Selecciona el nombre, apellidos y nombre del departamento actual de cada empleado
SELECT employees.first_name, employees.last_name, departments.dept_name FROM employees INNER JOIN dept_emp ON dept_emp.emp_no=employees.emp_no INNER JOIN departments ON departments.dept_no=dept_emp.dept_no WHERE dept_emp.to_date="2019-11-27"

Selecciona el nombre, apellidos y número de veces que el empleado a trabajado como manager
SELECT employees.first_name, employees.last_name, count(dept_manager.emp_no) FROM employees INNER JOIN dept_manager WHERE employees.emp_no=dept_manager.emp_no GROUP BY employees.first_name;

Selecciona el nombre sin que ninguno esté repetido
SELECT DISTINCT first_name FROM employees